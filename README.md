﻿Basic self-contained OWIN-based web server that I'm using to host my own tiny .NET thing on a 
Raspberry Pi using the Mono Framework. I actually find this easier than dealing with Apache, which
is something I know very very little about. This way I can develop everything I want in Visual Studio
and use a post-build script to push it out to the Pi.

This is the main tutorial I borrowed from:

https://richardniemand.wordpress.com/2015/06/18/hosting-web-api-and-static-content-with-owin/

Secondary tutorial:

http://j.tlns.be/2014/11/21/running-asp-net-on-a-raspberry-pi-with-mono-and-owin/