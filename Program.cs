﻿using System;
using Microsoft.Owin.Hosting;

namespace PiMonoWebServer
{
    class Program
    {
        static void Main(string[] args) {
            string baseUrl = @"http://*:80";
            var startupOptions = new StartOptions(baseUrl) {
                ServerFactory = "Microsoft.Owin.Host.HttpListener"
            };
            using (WebApp.Start<OwinStartup>(startupOptions))
            {
                Console.WriteLine("Press Enter to quit.");
                Console.ReadKey();
            }
        }
    }
}
